'use strict';

require('mocha');
const assert = require('assert');

/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
const removeNthFromEnd = function(head, n) {
  let right = head;

  for (let i = 0; i < n; i++) {
    right = right.next;
  }

  if (right === null) return head.next;

  let left = head;

  for (;;) {
    if (right.next === null) {
      left.next = left.next.next;
      return head;
    }

    left = left.next;
    right = right.next;
  }
};

describe(__filename, function() {
  it('should work', function() {
    function ListNode(val) {
      this.val = val;
      this.next = null;
    }

    function fromArray(arr) {
      var head = new ListNode(arr[0]);
      var cur = head;
      for (var i = 1; i < arr.length; i++) {
        cur.next = new ListNode(arr[i]);
        cur = cur.next;
      }
      return head;
    }

    function eq(head, arr) {
      for (var i = 0; i < arr.length; i++) {
        if (head === null) return false;
        if (head.val !== arr[i]) return false;
        head = head.next;
      }
      return true;
    }

    assert.equal(eq(removeNthFromEnd(fromArray([1,2,3,4,5]), 2), [1,2,3,5]), true);
    assert.equal(eq(removeNthFromEnd(fromArray([1,2]), 1), [1]), true);
    assert.equal(eq(removeNthFromEnd(fromArray([1,2,3,4,5]), 1), [1,2,3,4]), true);
    assert.equal(eq(removeNthFromEnd(fromArray([1,2,3,4,5]), 5), [2,3,4,5]), true);
  });
});
