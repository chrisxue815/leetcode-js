'use strict';

/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
const mergeTwoLists = function(l1, l2) {
  if (!l1) return l2;
  if (!l2) return l1;

  let head;

  if (l1.val < l2.val) {
    head = l1;
    l1 = l1.next;
  }
  else {
    head = l2;
    l2 = l2.next;
  }

  let cur = head;

  while (l1 && l2) {
    if (l1.val < l2.val) {
      cur.next = l1;
      cur = l1;
      l1 = l1.next;
    }
    else {
      cur.next = l2;
      cur = l2;
      l2 = l2.next;
    }
  }

  cur.next = l1 ? l1 : l2;

  return head;
};
