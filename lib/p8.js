'use strict';

require('mocha');
const assert = require('assert');

if (Number.MAX_SAFE_INT32 === undefined) {
  Number.MAX_SAFE_INT32 = 2147483647;
}

if (Number.MIN_SAFE_INT32 === undefined) {
  Number.MIN_SAFE_INT32 = -2147483648;
}

const zeroCharCode = '0'.charCodeAt(0);

/**
 * @param {string} str
 * @return {number}
 */
const myAtoi = function(str) {
  let ignoreSpace = true;
  let result = 0;
  let sign = 1;

  for (let i = 0; i < str.length; i++) {
    const ch = str[i];

    if (ch === ' ') {
      if (ignoreSpace) continue;
      else break;
    }
    else if (ch === '+') {
      if (ignoreSpace) {
        ignoreSpace = false;
        continue;
      }
      else {
        break;
      }
    }
    else if (ch === '-') {
      if (ignoreSpace) {
        ignoreSpace = false;
        sign = -1;
        continue;
      }
      else {
        break;
      }
    }

    var number = ch.charCodeAt(0) - zeroCharCode;
    if (number >= 0 && number <= 9) {
      ignoreSpace = false;
      result *= 10;
      result += number;
    }
    else {
      break;
    }
  }

  result *= sign;

  if (result > Number.MAX_SAFE_INT32) result = Number.MAX_SAFE_INT32;
  else if (result < Number.MIN_SAFE_INT32) result = Number.MIN_SAFE_INT32;

  return result;
};

describe(__filename, function() {
  it('should work', function() {
    assert.equal(myAtoi('123'), 123);
    assert.equal(myAtoi('+123'), 123);
    assert.equal(myAtoi('-123'), -123);
    assert.equal(myAtoi('   +123'), 123);
    assert.equal(myAtoi('   -123'), -123);
    assert.equal(myAtoi('123abc'), 123);
    assert.equal(myAtoi('999999999999'), Number.MAX_SAFE_INT32);
    assert.equal(myAtoi('-999999999999'), Number.MIN_SAFE_INT32);
  });
});
