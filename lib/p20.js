'use strict';

require('mocha');
const assert = require('assert');

/**
 * @param {string} s
 * @return {boolean}
 */
const isValid = function(s) {
  const stack = [];

  for (let i = 0; i < s.length; i++) {
    switch (s[i]) {
      case '(':
        stack.push(')');
        break;
      case '[':
        stack.push(']');
        break;
      case '{':
        stack.push('}');
        break;
      case ')':
      case ']':
      case '}':
        const ch = stack.pop();
        if (ch != s[i]) return false;
        break;
    }
  }

  return stack.length === 0;
};

describe(__filename, function() {
  it('should work', function() {
    assert.equal(isValid('()'), true);
    assert.equal(isValid('()[]{}'), true);
    assert.equal(isValid('([{}])'), true);
    assert.equal(isValid('('), false);
    assert.equal(isValid('(]'), false);
    assert.equal(isValid('([)]'), false);
  });
});
