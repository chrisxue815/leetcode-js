'use strict';

require('mocha');
const assert = require('assert');

if (Number.MAX_SAFE_INT32 === undefined) {
  Number.MAX_SAFE_INT32 = 2147483647;
}

if (Number.MIN_SAFE_INT32 === undefined) {
  Number.MIN_SAFE_INT32 = -2147483648;
}

/**
 * @param {number} x
 * @return {number}
 */
const reverse = function(x) {
  let result = 0;

  while (x !== 0) {
    result *= 10;
    if (result > Number.MAX_SAFE_INT32 || result < Number.MIN_SAFE_INT32) {
      return 0;
    }

    const remainder = x % 10;
    result += remainder;
    if (result > Number.MAX_SAFE_INT32 || result < Number.MIN_SAFE_INT32) {
      return 0;
    }

    x = ~~(x / 10);
  }

  return result;
};

/**
 * @param {number} x
 * @return {boolean}
 */
const isPalindrome = function(x) {
  return x >= 0 && reverse(x) === x;
};

describe(__filename, function() {
  it('should work', function() {
    assert.equal(isPalindrome(121), true);
    assert.equal(isPalindrome(12321), true);
    assert.equal(isPalindrome(1), true);
    assert.equal(isPalindrome(11), true);
    assert.equal(isPalindrome(-121), false);
    assert.equal(isPalindrome(123), false);
    assert.equal(isPalindrome(2147483647), false);
  });
});
