'use strict';

require('mocha');
const assert = require('assert');

if (Number.MAX_SAFE_INT32 === undefined) {
  Number.MAX_SAFE_INT32 = 2147483647;
}

if (Number.MIN_SAFE_INT32 === undefined) {
  Number.MIN_SAFE_INT32 = -2147483648;
}

/**
 * @param {number} x
 * @return {number}
 */
const reverse = function(x) {
  let result = 0;

  while (x !== 0) {
    result *= 10;
    if (result > Number.MAX_SAFE_INT32 || result < Number.MIN_SAFE_INT32) {
      return 0;
    }

    const remainder = x % 10;
    result += remainder;
    if (result > Number.MAX_SAFE_INT32 || result < Number.MIN_SAFE_INT32) {
      return 0;
    }

    // Using "number | 0" to cast number into integer
    x = (x / 10) | 0;
  }

  return result;
};

describe(__filename, function() {
  it('should work', function() {
    assert.equal(reverse(123), 321);
    assert.equal(reverse(-123), -321);
    assert.equal(reverse(1534236469), 0);
  });
});
