'use strict';

require('mocha');
const assert = require('assert');

/**
 * @param {string[]} strs
 * @return {string}
 */
const longestCommonPrefix = function(strs) {
  if (strs.length === 0) return '';
  if (strs.length === 1) return strs[0];

  let result = '';

  for (let i = 0;; i++) {
    if (i >= strs[0].length) return result;

    const ch = strs[0][i];

    for (let j = 1; j < strs.length; j++) {
      if (i >= strs[j].length) return result;

      if (ch !== strs[j][i]) return result;
    }

    result += ch;
  }
};

describe(__filename, function() {
  it('should work', function() {
    assert.equal(longestCommonPrefix(['12345', '12356']), '123');
    assert.equal(longestCommonPrefix(['12345', '12356', '1256']), '12');
  });
});
