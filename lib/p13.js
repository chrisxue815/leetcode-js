'use strict';

require('mocha');
const assert = require('assert');

/**
 * @param {string} s
 * @return {number}
 */
const romanToInt = function(s) {
  if (s.length === 0) return 0;

  const numerals = {
    I: 1,
    V: 5,
    X: 10,
    L: 50,
    C: 100,
    D: 500,
    M: 1000,
  };

  let result = 0;

  for (let i = 0; i < s.length; i++) {
    const cur = numerals[s[i]];
    let next = s[i + 1];
    next = next ? numerals[next] : 0;

    if (next === cur * 5 || next === cur * 10) {
      result -= cur;
    }
    else {
      result += cur;
    }
  }

  return result;
};

describe(__filename, function() {
  it('should work', function() {
    assert.equal(romanToInt('I'), 1);
    assert.equal(romanToInt('IV'), 4);
    assert.equal(romanToInt('MMMCMXCIX'), 3999);
    assert.equal(isNaN(romanToInt('ABC')), true);
  });
});
